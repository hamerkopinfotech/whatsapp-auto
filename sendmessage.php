<?php
require 'Classes/PHPExcel.php';
ini_set('max_execution_time', 30000); 
//echo shell_exec("cd C:\whatsappauto && mvn test");
    $errors = array();
    $status = array();
    
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    
       
        if(isset($_FILES['excel'])){
            $status = uploadFile($_FILES['excel'], 'excel');     
        } else {
            header('Location:index.php?error=filemissing');
        }
        if(isset($_FILES['image'])){
            $status = uploadFile($_FILES['image'], 'image');     
        }
        if(isset($_FILES['video'])){
            $status = uploadFile($_FILES['video'], 'video');     
        }
        if(isset($_FILES['pdf'])){
            $status = uploadFile($_FILES['video'], 'pdf');     
        }
        if(isset($_FILES['word'])){
            $status = uploadFile($_FILES['video'], 'word');     
        }
        
        if($_POST['select-file'] == 'message'){

            echo shell_exec("cd C:\whatsapp-auto-selenium && mvn -Dtest=projecttext test");
            
            
        } elseif($_POST['select-file'] == 'message-metatag'){
            
            echo shell_exec("cd C:\whatsapp-auto-selenium && mvn -Dtest=projectlink test");
            
        } elseif($_POST['select-file'] == 'image'){
            
            echo shell_exec("cd C:\whatsapp-auto-selenium && mvn -Dtest=projectimage test");
            
        } elseif($_POST['select-file'] == 'video'){
            
            echo shell_exec("cd C:\whatsapp-auto-selenium && mvn -Dtest=projectvideo test");
            
        } elseif($_POST['select-file'] == 'pdf'){
            
            echo shell_exec("cd C:\whatsapp-auto-selenium && mvn -Dtest=projectpdf test");
            
        } elseif($_POST['select-file'] == 'word'){
            
            echo shell_exec("cd C:\whatsapp-auto-selenium && mvn -Dtest=projectdoc test");
            
        }
        header('Location:index.php?status=success');
    }
   
   function uploadFile($file, $name){
       
      
      $file_name = $_FILES[$name]['name'];
      $file_size =$_FILES[$name]['size'];
      $file_tmp =$_FILES[$name]['tmp_name'];
      $file_type=$_FILES[$name]['type'];
      $file_ext=strtolower(end(explode('.',$_FILES[$name]['name'])));
      
      if(($name == "excel") &&($file_type !="")){

        try {
            $objPHPExcel = PHPExcel_IOFactory::load($file_tmp);
        } catch(Exception $e) {
            die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }


        $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        $arrayCount = count($allDataInSheet);  // Here get total count of row in that Excel sheet

        for($i=1;$i<=$arrayCount;$i++){
            if(!isset($allDataInSheet[$i]["B"])){
                header('Location:index.php?error=messagemissing');
                exit;
            }
      /*     $mobilenumber = trim($allDataInSheet[$i]["A"]);
            $message = trim($allDataInSheet[$i]["B"]);
            
           $mobilenumberValidity = preg_match('/^[0-9]{12}+$/', $mobilenumber);
           $prefix = substr($mobilenumber, 0, 3);
            if(($mobilenumberValidity == 0) && ($prefix != "971")){
                 header('Location:index.php?error=mobilenumber');
                 exit;
            }
      */
        } 
          
        $file_name = "excel.xls";
      }
      
      if($name == "image"){
        $file_name = "image.jpg";
      }
      
      if($name == "video"){
        $file_name = "video.mp4";
      }
      if($name == "pdf"){
        $file_name = "doc.pdf";
      } 
      if($name == "word"){
        $file_name = "word.docx";
      } 
      
      
//      if($file_size > 2097152){
//         $errors[]='File size must be excately 2 MB';
//      }
      
      if(empty($errors)==true){
          if(file_exists("media/$name/".$file_name)){
              unlink("media/$name/".$file_name);
          }
            move_uploaded_file($file_tmp,"media/$name/".$file_name);
      }else{
          
          return $errors; 
      }
       
   }

?>
