<?php

//require 'Classes/PHPExcel.php';
require_once 'Classes/PHPExcel.php';
require_once 'Classes/PHPExcel/IOFactory.php';
require_once 'Classes/PHPExcel/Writer/Excel5.php';
ini_set('max_execution_time', '30000');
//$command = "cd C:\whatsappauto && mvn test";

$errors = array();
$status = array();
$coloumns = '';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	print_r($_POST);
    if (isset($_FILES['excel'])) {
        $status = uploadFile($_FILES['excel'], 'excel');
    } else {
        header('Location:index.php?error=filemissing');
    }
    if (isset($_FILES['image'])) {
        $status = uploadFile($_FILES['image'], 'image');
    }
    if (isset($_FILES['video'])) {
        $status = uploadFile($_FILES['video'], 'video');
    }
    if (isset($_FILES['pdf'])) {
        $status = uploadFile($_FILES['pdf'], 'pdf');
    }
    if (isset($_FILES['word'])) {
        $status = uploadFile($_FILES['word'], 'word');
    }

    if ($_POST['text-select'] != '-1') {
        $text_type = $_POST['text-select'];

        if ($text_type == 'short') {
            $command = "cd C:\whatsapp-auto-selenium && mvn -Dtest=projectshorttext test";
        }
        if ($text_type == 'long') {
            $command = "cd C:\whatsapp-auto-selenium && mvn -Dtest=projectlongtext test";
        }
        if ($text_type == 'text-perfect') {
            $command = "cd C:\whatsapp-auto-selenium && mvn -Dtest=projecttextperfectnumber test";
        }
    } elseif ($_POST['image-select'] != '-1') {
        $image_size = $_POST['image-select'];

        if ($image_size == '5') {
            $command = "cd C:\whatsapp-auto-selenium && mvn -Dtest=projectimage5 test";
        }

        if ($image_size == '10') {
            $command = "cd C:\whatsapp-auto-selenium && mvn -Dtest=projectimage10 test";
        }

        if ($image_size == '20') {
            $command = "cd C:\whatsapp-auto-selenium && mvn -Dtest=projectimage20 test";
        }
    } elseif ($_POST['pdf-select'] != '-1') {
        $pdf_size = $_POST['pdf-select'];

        if ($pdf_size == '5') {
            $command = "cd C:\whatsapp-auto-selenium && mvn -Dtest=projectpdf5 test";
        }

        if ($pdf_size == '10') {
            $command = "cd C:\whatsapp-auto-selenium && mvn -Dtest=projectpdf10 test";
        }

        if ($pdf_size == '20') {
            $command = "cd C:\whatsapp-auto-selenium && mvn -Dtest=projectpdf20 test";
        }
    } elseif ($_POST['video-select'] != '-1') {
        $video_size = $_POST['video-select'];
        if ($video_size == '5') {
            $command = "cd C:\whatsapp-auto-selenium && mvn -Dtest=projectvideo5 test";
        }

        if ($video_size == '10') {
            $command = "cd C:\whatsapp-auto-selenium && mvn -Dtest=projectvideo10 test";
        }

        if ($video_size == '20') {
            $command = "cd C:\whatsapp-auto-selenium && mvn -Dtest=projectvideo20 test";
        }
    } elseif ($_POST['word-select'] != '-1') {
        $word_size = $_POST['word-select'];
        if ($word_size == '5') {
            $command = "cd C:\whatsapp-auto-selenium && mvn -Dtest=projectword5 test";
        }

        if ($word_size == '10') {
            $command = "cd C:\whatsapp-auto-selenium && mvn -Dtest=projectword10 test";
        }

        if ($word_size == '20') {
            $command = "cd C:\whatsapp-auto-selenium && mvn -Dtest=projectword20 test";
        }
    }
    try {
        $shell_return = "";
        $shell_return = shell_exec($command . " 2>&1");
        $shell_return = substr($shell_return, 0, 5);
        if ($shell_return == "[INFO") {
            header('Location:custom-index.php?status=stopped');
            return;
        }
    } catch (Exception $e) {

        header('Location:custom-index.php?status=stopped');
    }
    header('Location:custom-index.php');
}

function uploadFile($file, $name) {

    $file_name = $_FILES[$name]['name'];
    $file_size = $_FILES[$name]['size'];
    $file_tmp = $_FILES[$name]['tmp_name'];
    $file_type = $_FILES[$name]['type'];
    $file_ext = strtolower(end(explode('.', $_FILES[$name]['name'])));
    if (($name == "excel") && ($file_type != "")) {
        $file_name = "excel.xls";
    }

    if ($name == "image") {
        $file_name = "image.jpg";
    }

    if ($name == "video") {
        $file_name = "video.mp4";
    }
    if ($name == "pdf") {
        $file_name = "doc.pdf";
    }
    if ($name == "word") {
        $file_name = "word.docx";
    }

//      if($file_size > 2097152){
//         $errors[]='File size must be excately 2 MB';
//      }

    if (empty($errors) == true) {
        if (file_exists("media/$name/" . $file_name)) {
            unlink("media/$name/" . $file_name);
        }
        move_uploaded_file($file_tmp, "media/$name/" . $file_name);
    } else {

        return $errors;
    }
}

?>
