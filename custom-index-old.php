<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Imdad</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--===============================================================================================-->
        <!--	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>-->
        <link rel="icon" type="image/png" href="images/icons/favicon.png"/>
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="css/util.css">
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <!--===============================================================================================-->
    </head>
    <style>
        .blink{
            color:red;
            font-size:15px;
            animation:blink_animation .5s infinite;
        }
        @keyframes blink_animation {
            50%   {color:green;}      
            100% {color:grey}
        }
    </style>
    <!--<body onload="window.resizeTo(640,700)">-->
    <script type="text/javascript">

        function convertlink() {
            window.location = "http://localhost/whatsapp-auto/convert-custom-file.php";
        }
    </script>
    <body>

        <div class="contact1">

            <div class="container-contact1">

                <div class="contact1-pic js-tilt" data-tilt>
                    <img src="images/img-01.png" alt="IMG">
                </div>
                <div id="total-container">
                    <span class="contact1-form-title">
                        <img src="images/whatsapp.png" alt="IMG">
                        Whatsapp Auto
                    </span>

                    <div class="alert alert-success" id="message-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Success!</strong> Message Sent.
                    </div>
                    <div class="alert alert-danger" id="message-missing">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Error!</strong> Message missing for contacts.
                    </div>
                    <div class="alert alert-danger" id="message-sending-failed">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Error!</strong> Message sending failed or server error, try again.
                    </div>
                    <div class="alert alert-danger" id="sending-stopped">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Sending Stopped!</strong>Unexpected server error, try again.
                    </div>


                    <div class="alert alert-warning" id="message-sending-warning" style="width: 500px;">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Warning!</strong> Don't reload or close the page, Whatsapp will open in new tab <b>in few seconds</b>, scan QR-Code to continue.
                    </div>

                    <div class="container-contact1-form-btn" style="margin-right: 200px;margin-bottom: 50px;" onclick="convertlink()">

                        <button class="contact1-form-btn" id="convert-custom-excel" >
                            <span>
                                Convert Custom Excel
                                <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                            </span>
                        </button>
                    </div>

                    <form id="sendmessage" class="contact1-form validate-form" action="custom-sendmessage.php" method="post" enctype="multipart/form-data">


                        <div id="normal-excel">
                            <div class="wrap-input1 validate-input">
                                <div style="float:left;">Upload Normal Excel</div>
                                <div style="float:right;"><a href="#"  onclick="window.open('Sample-File.xls')"><b>Download Sample File</b></a>&nbsp;|&nbsp;<a href="#"  onclick="window.open('Help.docx', '_blank');"><b>Help</b></a></div>

                                <input id="excel" class="input1" type="file" name="excel" placeholder="Contacts">

                                <span class="shadow-input1">
                                </span>
                                <div style="color:red" id="contact-error"><br>

                                </div>
                                <div style="color:#b2beb5; font-size:15px"><i>*Upload normal excel in xls format</i></div>
                                <br>
                            </div>

                        </div>

                        <div id="final-execution">
                            <div class="wrap-input1 validate-input">
                                <br>
                                <b>*<i>Send text and links Only</i></b>
                                <br>
                                <select id="text-select" name="text-select" class="form-control">
                                    <option value="-1">-Select Text Type-</option>
                                    <option value="short">Short Text</option>
                                    <option value="long">Long Text</option>
                                </select>
                                <div style="color:red" id="text-error">

                                </div>
                                <br>
                                <b><i>Send Image with text</i></b>
                                <br>
                                <select id="image-select" name="image-select" class="form-control">
                                    <option value="-1">-Select File Size Range-</option>
                                    <option value="5">0 to 5MB</option>
                                    <option value="10">5 to 10MB</option>
                                    <option value="20">10 to 20MB</option>
                                </select>

                                <div id="image-section" class="wrap-input1 validate-input">
                                    <input id="image" class="input1" type="file" name="image" placeholder="Image">
                                    <span class="shadow-input1"></span>
                                    <div style="color:#b2beb5"><i>(Upload image in jpg,jpeg or png format)</i></div>
                                    <div style="color:red" id="image-error">
                                    </div>
                                </div>
								<br>
                                <b>*<i>Send Video</i></b>
                                <br>
                                <select id="video-select" name="video-select" class="form-control">
                                    <option value="-1">-Select File Size Range-</option>
                                    <option value="5">0 to 5MB</option>
                                    <option value="10">5 to 10MB</option>
                                    <option value="20">10 to 20MB</option>
                                </select>
                                <div id="video-section" class="wrap-input1 validate-input">
                                    <input id="video" class="input1" type="file" name="video" placeholder="Video">
                                    <span class="shadow-input1"></span>
                                    <div style="color:#b2beb5"><i>(Upload video in mp4 format)</i></div>
                                    <div style="color:red" id="video-error">
                                    </div>
                                </div>
                                <br>
                                <b>*<i>Send PDF Only</i></b>
                                <br>
                                <select id="pdf-select" name="pdf-select" class="form-control">
                                    <option value="-1">-Select File Size Range-</option>
                                    <option value="5">0 to 5MB</option>
                                    <option value="10">5 to 10MB</option>
<!--                                    <option value="20">10 to 20MB</option>-->
                                </select>
                                <br>
                                <div id="pdf-section" class="wrap-input1 validate-input">
                                    <input id="pdf" class="input1" type="file" name="pdf" placeholder="pdf">
                                    <span class="shadow-input1"></span>
                                    <div style="color:#b2beb5"><i>(Upload file in pdf format)</i></div>
                                    <div style="color:red" id="pdf-error">
                                    </div>
                                </div>
                                <b>*<i>Send Word Only</i></b>
                                <br>
                                <select id="word-select" name="word-select" class="form-control">
                                    <option value="-1">-Select File Size Range-</option>
                                    <option value="5">0 to 5MB</option>
                                    <!--<option value="10">5 to 10MB</option>
                                    <option value="20">10 to 20MB</option>-->
                                </select>
                                <div id="word-section" class="wrap-input1 validate-input">
                                    <input id="word" class="input1" type="file" name="word" placeholder="Word">
                                    <span class="shadow-input1"></span>
                                    <div style="color:#b2beb5"><i>(Upload file in docx format)</i></div>
                                    <div style="color:red" id="word-error">
                                    </div>
                                </div>


                            </div>




                            <div class="wrap-input1 validate-input" data-validate = "Message is required">
                                <textarea id="message" class="input1" name="message" placeholder="Message"></textarea>
                                <span class="shadow-input1"></span>
                            </div>

                            <div class="container-contact1-form-btn" style="margin-right: 250px;margin-top: 50px;">

                                <button id="contact1-form-btn" class="contact1-form-btn" >
                                    <span>
                                        Send Message
                                        <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>




        <!--===============================================================================================-->
        <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/bootstrap/js/popper.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/select2/select2.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/tilt/tilt.jquery.min.js"></script>
        <script >
                                        $('.js-tilt').tilt({
                                            scale: 1.1
                                        })
        </script>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
        <script>
    window.dataLayer = window.dataLayer || [];
    function gtag() {
        dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-23581568-13');



    $(document).ready(function () {
        $("#message-success").css("display", "none");
        $("#message-missing").css("display", "none");
		$("#sending-stopped").css("display", "none");
        $("#message-sending-failed").css("display", "none");
        $("#message-sending-warning").css("display", "none");
        $("#image-section").css("display", "none");
       // $("#text-error").css("display", "none");
        $("#pdf-section").css("display", "none");
        $("#word-section").css("display", "none");
        $("#video-section").css("display", "none");
        $("#message").css("display", "none");
        $("#contact-error").text('');
        $("#coloumn-error").text('');
        $("#image-error").text('');
        $("#video-error").text('');

        $('select').change(function (event) {
            var id = $(this).attr("id");
            if (id == "pdf-select") {
                $('#text-select').val('-1');
                $('#image-select').val('-1');
                $('#video-select').val('-1');
                $('#word-select').val('-1');
                $('#image-section').css("display", "none");
                $('#video-section').css("display", "none");
                $('#pdf-section').css("display", "block");
                $('#word-section').css("display", "none");
            }
            if (id == "word-select") {
                $('#text-select').val('-1');
                $('#image-select').val('-1');
                $('#video-select').val('-1');
                $('#pdf-select').val('-1');

                $('#image-section').css("display", "none");
                $('#video-section').css("display", "none");
                $('#pdf-section').css("display", "none");
                $('#word-section').css("display", "block");
            }

            if (id == "image-select") {
                $('#text-select').val('-1');
                $('#word-select').val('-1');
                $('#video-select').val('-1');
                $('#pdf-select').val('-1');

                $('#image-section').css("display", "block");
                $('#video-section').css("display", "none");
                $('#pdf-section').css("display", "none");
                $('#word-section').css("display", "none");
            }
            if (id == "video-select") {
                $('#word-select').val('-1');
                $('#image-select').val('-1');
                $('#pdf-select').val('-1');
                $('#text-select').val('-1');
                $('#video-section').css("display", "block");
                $('#image-section').css("display", "none");
                $('#pdf-section').css("display", "none");
                $('#word-section').css("display", "none");
            }
            if (id == "text-select") {
                $('#word-select').val('-1');
                $('#image-select').val('-1');
                $('#video-select').val('-1');
                $('#pdf-select').val('-1');

                $('#video-section').css("display", "none");
                $('#image-section').css("display", "none");
                $('#pdf-section').css("display", "none");
                $('#word-section').css("display", "none");
                $('#word-section').css("display", "none");
                $("#text-error").css("display", "none");

            }
        });



        var url_string = window.location.href;
        var url = new URL(url_string);
        var error = url.searchParams.get("error");
        var status = url.searchParams.get("status");
        /* if(status == "success"){
         $("#message-success").css("display","block"); 
         }*/
        if (status == "error") {
            $("#message-sending-failed").css("display", "block");
        }
        if (status == "stopped") {
            $("#sending-stopped").css("display", "block");
        }
        if (error == "mobilenumber") {
            $("#contact-error").text("Upload excel with valid mobile number");
        }
        if (error == "messagemissing") {
            $("#message-missing").css("display", "block");
        }
        if (error == "wrongcoloumn") {
            $("#excel-type-custom").trigger("click");
            $("#wrong-coloumn").css("display", "block");
        }
        $("#contact1-form-btn").click(function () {
            //e.preventDefault(); 
            

            if ($("#excel").val() == "") {
                 document.getElementById('excel').focus();
                $("#contact-error").text('* Please upload contacts');
                
                return false;
            } else {
                var str = $("#excel").val();
                var strArr = str.split('.');
                if (strArr[1] != 'xls') {
                     document.getElementById('excel').focus();
                    $("#contact-error").text('* Please upload excel in xls format');
                    
                    
                    return false;
                } else {
                    $("#contact-error").text('');
                }
            }
            if ($('#image-select').val() != '-1') {
                if ($("#image").val() != '') {
                    var str = $("#image").val();
                    var strArr = str.split('.');
                    if (strArr[1] != 'jpg' && strArr[1] != 'jpeg' && strArr[1] != 'png') {
                         document.getElementById('image').focus();
                        $("#image-error").text('* Please upload an image in jpg, png or jpeg format');
                        return false;
                    } else {
                        $("#image-error").text('');
                    }
                } else {
                    document.getElementById('image').focus();
                    $("#image-error").text('* Upload Image');
                    return false;
                }
            }
            else if ($('#video-select').val() != '-1') {
                if ($("#video").val() != '') {
                    var str = $("#video").val();
                    // var strArr = str.split('.');
                    var extension = str.substr(str.length - 3);
                    if (extension != 'mp4') {
                        document.getElementById('video').focus();
                        $("#video-error").text('* Please upload an video file in mp4 format');
                        return false;
                    } else {
                        $("#video-error").text('');
                    }
                } else {
                    document.getElementById('video').focus();
                    $("#video-error").text('* Upload Video');
                    return false;
                }
            }

            else if ($('#pdf-select').val() != '-1') {
                if ($("#pdf").val() != '') {
                    var str = $("#pdf").val();
                    var strArr = str.split('.');
                    if (strArr[1] != 'pdf') {
                        document.getElementById('pdf').focus();
                        $("#pdf-error").text('* Please upload file in pdf format');
                        return false;
                    } else {
                        $("#pdf-error").text('');
                    }
                } else {
                    document.getElementById('pdf').focus();
                    $("#pdf-error").text('* Upload PDF');
                    return false;
                }
            }


            else if ($('#word-select').val() != '-1') {
                if ($("#word").val() != '') {
                    var str = $("#word").val();
                    var strArr = str.split('.');
                    if (strArr[1] != 'docx' && strArr[1] != 'doc') {
                        document.getElementById('word').focus();
                        $("#word-error").text('* Please upload file in doc or docx format');
                        return false;
                    } else {
                        $("#word-error").text('');
                    }
                } else {
                    document.getElementById('word').focus();
                    $("#word-error").text('* Upload document');
                    return false;
                }
            }
            else if ($('#word-select').val() != '-1') {
                if ($("#word").val() != '') {
                    var str = $("#word").val();
                    var strArr = str.split('.');
                    if (strArr[1] != 'docx') {
                        document.getElementById('word').focus();
                        $("#word-error").text('* Please upload word in docx');
                        return false;
                    } else {
                        $("#image-error").text('');
                    }
                } else {
                    document.getElementById('word').focus();
                    $("#word-error").text('* Upload Word');
                    return false;
                }
            } else {
            
            
            if ($('#text-select').val() == '-1') {
                document.getElementById('text-select').focus();
                $("#text-error").text('* Please select a text type');
                return false;
            } else {
                $("#text-error").text('');
            }
        }

            $(".contact1-form-btn").css("display", "none");
            //$("#reset").css("display","block");
            document.getElementById('message-sending-warning').focus();
            $("#message-sending-warning").css("display", "block");
            $("#sendmessage").submit();
            $("#message-success").css("display", "block");



        });
    });
        </script>

        <!--===============================================================================================-->
        <script src="js/main.js"></script>

    </body>
</html>
