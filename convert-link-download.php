<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Imdad</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--===============================================================================================-->
        <!--	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>-->
        <link rel="icon" type="image/png" href="images/icons/favicon.png"/>
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="css/util.css">
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <!--===============================================================================================-->
    </head>
    <style>
        .blink{
            color:red;
            font-size:15px;
            animation:blink_animation .5s infinite;
        }
        @keyframes blink_animation {
            50%   {color:green;}
            100% {color:grey}
        }
    </style>
    <!--<body onload="window.resizeTo(640,700)">-->
    <script type="text/javascript">

        function back() {
            window.location = "http://localhost/whatsapp-auto/convert-custom-file.php";
        }
        function next() {
            window.location = "http://localhost/whatsapp-auto/custom-index.php";

        }
    </script>
    <body>

        <div class="contact1">

            <div class="container-contact1" >


                <div style="width: 1043px;align-self: center;">
                    <span class="contact1-form-title" style="float: left;">
                        <h5>Send WhatsApp Message</h5>
                    </span>
                    <span class="contact1-form-title" style="float: right;">
                        <img src="images/whatsapp.png" alt="IMG">
                        Imdad WhatsApp Auto
                    </span>
                    <div style="margin-bottom: 20px;clear: both;border-bottom: 1px solid #00c1ff;">&nbsp;</div>

                    <div class="wrap-input1 validate-input">
                        <div style="color:#57b846;font-size:22px;"><b>Success!</b> Successfully generated the excel file in sample format. Thank you.</div>
                        <div style="color:#657568;font-size:18px;line-height: 36px;padding-top: 20px;width: 1043px;">
                            <i>1.&nbsp; <a class="blink" href="#" style="font-size:18px;" onclick="window.open('media/excel/excel.xls')"><b>Click Here</b></a> to download the excel file generated.</i>
                            <br>
                            <i>2.&nbsp; Check the downloaded file, and confirm the mobile and messages are correct.</i>
                            <br>
                            <i>3.&nbsp; It's important to save and close the file before sending the WhatsApp messages to contacts in the file.</i>
                            <br>
                            <i>4.&nbsp; Now try the WhatsApp message sending after choosing the file generated.</i>
                        </div>
                        <button style="margin-top: 20px;" id="convert-contact-btn" class="contact1-form-btn" onclick="next();">
                            <span>
                                Send WhatsApp Messages
                                <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--===============================================================================================-->
    <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/bootstrap/js/popper.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/select2/select2.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/tilt/tilt.jquery.min.js"></script>
    <script >
                            $('.js-tilt').tilt({
                                scale: 1.1
                            })
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
    <script>
                            window.dataLayer = window.dataLayer || [];
                            function gtag() {
                                dataLayer.push(arguments);
                            }
                            gtag('js', new Date());

                            gtag('config', 'UA-23581568-13');
    </script>

    <!--===============================================================================================-->
    <script src="js/main.js"></script>

</body>
</html>
