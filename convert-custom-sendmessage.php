<?php
require_once 'Classes/PHPExcel.php';
require_once 'Classes/PHPExcel/IOFactory.php';
require_once 'Classes/PHPExcel/Writer/Excel5.php';
ini_set('max_execution_time', 30000); 

    $errors = array();
    $status = array();
    $coloumns = '';
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    
        $coloumns = $_POST['mobile-coloumn'].','.$_POST['message-coloumn'];
        if(isset($_FILES['excel'])){
            $status = uploadFile($_FILES['excel'], 'excel', $coloumns);   
        } else {
            header('Location:convert-custom-file.php?error=filemissing');
        }
        header('Location:convert-link-download.php');
    }
   
   function uploadFile($file, $name, $coloumns){
      $mobile_column = "";
      $message_column = "";
      $coloumn = array();
      $mobilenumberArr = array();
      $messageArr = array();
      $mobile_column = "";
      $message_column = "";
      $file_name = $_FILES[$name]['name'];
      $file_size =$_FILES[$name]['size'];
      $file_tmp =$_FILES[$name]['tmp_name'];
      $file_type=$_FILES[$name]['type'];
      $file_ext=strtolower(end(explode('.',$_FILES[$name]['name'])));
      $coloumn = explode(',',$coloumns);
      $mobile_column = $coloumn[0];
      $message_column = $coloumn[1];

        try {
            $objPHPExcel = PHPExcel_IOFactory::load($file_tmp);
        } catch(Exception $e) {
            die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }

         
        $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        $arrayCount = count($allDataInSheet);  // Here get total count of row in that Excel sheet
if($allDataInSheet[0]["$mobile_column"] != ''){
    header('Location:custom-index.php?error=wrongcoloumn');
    exit;
} elseif($allDataInSheet[0]["$message_column"]!=''){
    header('Location:custom-index.php?error=wrongcoloumn');
    exit;
}

        for($i=1;$i<=$arrayCount;$i++){
            $mobilenumberArr[] = trim($allDataInSheet[$i]["$mobile_column"]);
            $messageArr[] = trim($allDataInSheet[$i]["$message_column"]);
        }
//==================Excel-Write==========================//        
$file_permenant = "media/excel/excel.xls";


if(file_exists($file_permenant)){
    unlink($file_permenant);
}
  copy("excel.xls",$file_permenant);


$objPHPExcel = PHPExcel_IOFactory::load($file_permenant);

//$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Imdad")
                             ->setLastModifiedBy("Imdad")
                             ->setTitle("Office XLS Document");
        
for($i=1; $i<count($mobilenumberArr); $i++){
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $mobilenumberArr[$i]);
        $objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $messageArr[$i]);
}        
$objPHPExcel->getActiveSheet()->setTitle('Sheet1');
$objPHPExcel->setActiveSheetIndex(0);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="excel.xls"');
header('Cache-Control: max-age=0');  


$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save($file_permenant);
$objPHPExcel->disconnectWorksheets();
unset($objWriter, $objPHPExcel);
//==================Excel-Write==========================// 
       
   }

?>
