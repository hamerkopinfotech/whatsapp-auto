<!DOCTYPE html>
<html lang="en">
<head>
	<title>Imdad</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
<!--	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>-->
	<link rel="icon" type="image/png" href="images/icons/favicon.png"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<!--<body onload="window.resizeTo(640,700)">-->
<body>

	<div class="contact1">

		<div class="container-contact1">
                    
			<div class="contact1-pic js-tilt" data-tilt>
				<img src="images/img-01.png" alt="IMG">
			</div>

			<form id="sendmessage" class="contact1-form validate-form" action="sendmessage.php" method="post" enctype="multipart/form-data">
				<span class="contact1-form-title">
					<img src="images/whatsapp.png" alt="IMG">
					Whatsapp Auto
				</span>
                             
                                        <div class="alert alert-success" id="message-success">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            <strong>Success!</strong> Message Sent.
                                        </div>
                                        <div class="alert alert-danger" id="message-missing">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            <strong>Error!</strong> Message missing for contacts.
                                        </div>
                                        <div class="alert alert-danger" id="message-sending-failed">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            <strong>Error!</strong> Message sending failed or server error, try again.
                                        </div>
                                        <div class="alert alert-warning" id="message-sending-warning">
                                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                          <strong>Warning!</strong> Don't reload or close the page, Whatsapp will open in new tab <b>in few seconds</b>, scan QR-Code to continue.
                                        </div>
                                <div class="wrap-input1 validate-input">
                                    <div style="float:left;">Upload Excel</div>
                                    <div style="float:right;"><a href="#"  onclick="window.open('Sample-File.xls')"><b>Download Sample File</b></a>&nbsp;|&nbsp;<a href="#"  onclick="window.open('Help.docx','_blank');"><b>Help</b></a></div>
                                    
					<input id="excel" class="input1" type="file" name="excel" placeholder="Contacts">
               
                                        <span class="shadow-input1">
                                        </span>
                                        <div style="color:#b2beb5; font-size:15px"><i>*Upload excel in xls format</i></div>
										<!--<div style="color:#b2beb5"><i>*Mobile number and message feild should not be blank</i></div>
										<div style="color:#b2beb5"><i>*Mobile number should be given with country code eg:971557421145</i></div>-->
                                        <div style="color:red" id="contact-error">
                                        </div>
                                </div>
                            
                                <div class="wrap-input1 validate-input">
                                        
                                        <input id="none-radio" type="radio" name="select-file" value="message">
                                        Send Message in Text Only&nbsp;
                                        <br>
                                       
					<input id="image-radio" type="radio" name="select-file" value="image">  
                                         Send Message with Image and Text &nbsp;
                                         <br>
                                        <div id="image-section" class="wrap-input1 validate-input">
					<input id="image" class="input1" type="file" name="image" placeholder="Image">
					<span class="shadow-input1"></span>
                                        <div style="color:#b2beb5"><i>(Upload image in jpg,jpeg or png format)</i></div>
                                        <div style="color:red" id="image-error">
                                        </div>
                                        </div>
					<input id="video-radio" type="radio" name="select-file" value="video">
                                        Send Message with Video and Text
                                        <br>
                                        <div id="video-section" class="wrap-input1 validate-input">
					<input id="video" class="input1" type="file" name="video" placeholder="Video">
					<span class="shadow-input1"></span>
                                        <div style="color:#b2beb5"><i>(Upload video in mp4 format)</i></div>
                                        <div style="color:red" id="video-error">
                                        </div>
				</div>
                                        
                                </div>




				<div class="wrap-input1 validate-input" data-validate = "Message is required">
					<textarea id="message" class="input1" name="message" placeholder="Message"></textarea>
					<span class="shadow-input1"></span>
				</div>

				<div class="container-contact1-form-btn">
                                    
					<button class="contact1-form-btn">
						<span>
							Send Message
							<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
						</span>
					</button>
<!--                                        <button id="reset">
						<span>
							Reset
							<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
						</span>
					</button>-->
				</div>
			</form>
		</div>
	</div>




<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/tilt/tilt.jquery.min.js"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
  
  
  
  $(document).ready(function() {   
    $("#message-success").css("display","none");
    $("#message-missing").css("display","none");
    //$("#reset").css("display","none");
    $("#message-sending-failed").css("display","none");
    $("#message-sending-warning").css("display","none");
    $("#image-section").css("display","none");
    $("#video-section").css("display","none");
    $('#none-radio').attr('checked',true);
    $("#message").css("display","none");
    $("#contact-error").text('');
    $("#image-error").text('');
    $("#video-error").text('');
    
    $("input[type=radio]").click(function (event) {
        var id = $(this).attr("id");
        if(id == "image-radio"){
            $('#image-section').css("display","block");
            $('#video-section').css("display","none");
        }
         if(id == "video-radio"){
            $('#video-section').css("display","block");
            $('#image-section').css("display","none");
        } 
        if(id == "none-radio"){
            $('#video-section').css("display","none");
            $('#image-section').css("display","none");
        }  
    });
    
    
    
    var url_string = window.location.href;
    var url = new URL(url_string);
    var error = url.searchParams.get("error");
    var status = url.searchParams.get("status");
   /* if(status == "success"){
       $("#message-success").css("display","block"); 
    }*/
    if(status == "error"){
       $("#message-sending-failed").css("display","block"); 
    }
    if(error == "mobilenumber"){
       $("#contact-error").text("Upload excel with valid mobile number"); 
    }
    if(error == "messagemissing"){
       $("#message-missing").css("display","block"); 
    }
    $(".contact1-form-btn").click(function(){
        //e.preventDefault(); 
        if($("#excel").val() == ""){
            $("#contact-error").text('* Please upload contacts');
            return false;
        } else{ 
            var str = $("#excel").val();
            var strArr = str.split('.');     
             if(strArr[1] != 'xls'){
                 $("#contact-error").text('* Please upload excel in xls format');
                 return false;
             } else{
                 $("#contact-error").text('');
             }
        }
       if($('#image-radio').is(':checked')) {  
            if($("#image").val() != ''){
                var str = $("#image").val();
                var strArr = str.split('.');     
                 if(strArr[1] != 'jpg' && strArr[1] != 'jpeg'&& strArr[1] != 'png'){
                     $("#image-error").text('* Please upload an image in jpg, png or jpeg format');
                     return false;
                 } else {
                     $("#image-error").text('');
                 }
            } else {
                $("#image-error").text('* Upload Image');
                return false;
            }
        }
        if($('#video-radio').is(':checked')) {      
            if($("#video").val() != ''){
                var str = $("#video").val();
                var strArr = str.split('.');     
                 if(strArr[1] != 'mp4'){
                     $("#video-error").text('* Please upload an video file in mp4 format');
                     return false;
                 }  else {
                     $("#video-error").text('');
                 }
            } else {
                $("#video-error").text('* Upload Video');
                return false;
            }
        }  
        $(".contact1-form-btn").css("display","none");
        //$("#reset").css("display","block");
        $("#message-sending-warning").css("display","block");
        $("#sendmessage").submit();
        $("#message-success").css("display","block"); 

        
       
});
//$("#reset").click(function(){
//    location.reload();
//});
});
</script>

<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>
