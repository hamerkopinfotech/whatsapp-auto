<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Imdad WhatsApp Auto</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--===============================================================================================-->
        <!--	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>-->
        <link rel="icon" type="image/png" href="images/icons/favicon.png"/>
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="css/util.css">
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <!--===============================================================================================-->
    </head>
    <style>
        .blink{
            color:red;
            font-size:15px;
            animation:blink_animation .5s infinite;
        }
        @keyframes blink_animation {
            50%   {color:green;}
            100% {color:grey}
        }

    </style>
    <script type="text/javascript">

        function back() {
            window.location = "http://localhost/whatsapp-auto/custom-index.php";
        }
    </script>
    <!--<body onload="window.resizeTo(640,700)">-->
    <body>

        <div class="contact1">

            <div class="container-contact1">

                <div style="width: 1043px;">
                    <span class="contact1-form-title" style="float: left;">
                        <h5>Generate Excel from Source</h5>
                    </span>
                    <span class="contact1-form-title" style="float: right;">
                        <img src="images/whatsapp.png" alt="IMG">
                        Imdad WhatsApp Auto
                    </span>
                    <div style="margin-bottom: 20px;clear: both;border-bottom: 1px solid #00c1ff;">&nbsp;</div>
                    <div class="row">

                        <div class="col-sm-6">
                            <div class="alert alert-danger" id="wrong-coloumn" style="width: 500px;">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Error!</strong> Conversion failed ,select proper column or server error, try again.
                            </div>
                            <div id="custom-excel">

                                <form id="convertcustom" class="contact1-form validate-form" action="convert-custom-sendmessage.php" method="post" enctype="multipart/form-data">
                                    <div class="wrap-input1 validate-input">
                                        <div style="float:left;"><b>Choose the Excel File *</b></div>
                                        <input id="convert-excel" class="input1" type="file" name="excel" placeholder="Contacts">
                                        <span class="shadow-input1"></span>
                                        <div style="color:red;font-weight: bold" id="convert-contact-error"></div>
                                    </div>
                                    <div class="wrap-input1 validate-input">
                                        <div class="form-group">
                                            <select id="mobile-coloumn" name="mobile-coloumn" class="form-control">
                                                <option value="">Select the Mobile Column to Map</option>
                                                <option value="A">A</option>
                                                <option value="B">B</option>
                                                <option value="C">C</option>
                                                <option value="D">D</option>
                                                <option value="E">E</option>
                                                <option value="F">F</option>
                                                <option value="G">G</option>
                                                <option value="H">H</option>
                                                <option value="I">I</option>
                                                <option value="J">J</option>
                                                <option value="K">K</option>
                                                <option value="L">L</option>
                                                <option value="M">M</option>
                                                <option value="N">N</option>
                                                <option value="O">O</option>
                                                <option value="P">P</option>
                                                <option value="Q">Q</option>
                                                <option value="R">R</option>
                                                <option value="S">S</option>
                                                <option value="T">T</option>
                                                <option value="U">U</option>
                                                <option value="V">V</option>
                                                <option value="W">W</option>
                                                <option value="X">X</option>
                                                <option value="Y">Y</option>
                                                <option value="Z">Z</option>
                                                <option value="AA">AA</option>
                                                <option value="AB">AB</option>
                                                <option value="AC">AC</option>
                                                <option value="AD">AD</option>
                                                <option value="AE">AE</option>
                                                <option value="AF">AF</option>
                                                <option value="AG">AG</option>
                                                <option value="AH">AH</option>
                                                <option value="AI">AI</option>
                                                <option value="AJ">AJ</option>
                                                <option value="AK">AK</option>
                                                <option value="AL">AL</option>
                                                <option value="AM">AM</option>
                                                <option value="AN">AN</option>
                                                <option value="AO">AO</option>
                                                <option value="AP">AP</option>
                                                <option value="AQ">AQ</option>
                                                <option value="AR">AR</option>
                                                <option value="AS">AS</option>
                                                <option value="AT">AT</option>
                                                <option value="AU">AU</option>
                                                <option value="AV">AV</option>
                                                <option value="AW">AW</option>
                                                <option value="AX">AX</option>
                                                <option value="AY">AY</option>
                                                <option value="AZ">AZ</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <select id="message-coloumn" name="message-coloumn"  class="form-control">
                                                <option value="">Select the Message Column to Map</option>
                                                <option value="A">A</option>
                                                <option value="B">B</option>
                                                <option value="C">C</option>
                                                <option value="D">D</option>
                                                <option value="E">E</option>
                                                <option value="F">F</option>
                                                <option value="G">G</option>
                                                <option value="H">H</option>
                                                <option value="I">I</option>
                                                <option value="J">J</option>
                                                <option value="K">K</option>
                                                <option value="L">L</option>
                                                <option value="M">M</option>
                                                <option value="N">N</option>
                                                <option value="O">O</option>
                                                <option value="P">P</option>
                                                <option value="Q">Q</option>
                                                <option value="R">R</option>
                                                <option value="S">S</option>
                                                <option value="T">T</option>
                                                <option value="U">U</option>
                                                <option value="V">V</option>
                                                <option value="W">W</option>
                                                <option value="X">X</option>
                                                <option value="Y">Y</option>
                                                <option value="Z">Z</option>
                                                <option value="AA">AA</option>
                                                <option value="AB">AB</option>
                                                <option value="AC">AC</option>
                                                <option value="AD">AD</option>
                                                <option value="AE">AE</option>
                                                <option value="AF">AF</option>
                                                <option value="AG">AG</option>
                                                <option value="AH">AH</option>
                                                <option value="AI">AI</option>
                                                <option value="AJ">AJ</option>
                                                <option value="AK">AK</option>
                                                <option value="AL">AL</option>
                                                <option value="AM">AM</option>
                                                <option value="AN">AN</option>
                                                <option value="AO">AO</option>
                                                <option value="AP">AP</option>
                                                <option value="AQ">AQ</option>
                                                <option value="AR">AR</option>
                                                <option value="AS">AS</option>
                                                <option value="AT">AT</option>
                                                <option value="AU">AU</option>
                                                <option value="AV">AV</option>
                                                <option value="AW">AW</option>
                                                <option value="AX">AX</option>
                                                <option value="AY">AY</option>
                                                <option value="AZ">AZ</option>
                                            </select>
                                        </div>
                                        <div style="color:red;font-weight: bold" id="coloumn-error"></div>
                                    </div>
                                    <div style="width: 180px;float: left;">
                                        <a id="contact-btn-back" class="contact1-form-btn" onclick="back();" style="color: #fff;cursor: pointer;">
                                            <span>
                                                <i class="fa fa-long-arrow-left" aria-hidden="true"></i>
                                                Back
                                            </span>
                                        </a>
                                    </div>
                                    
                                    <div class="container-contact1-form-btn">
                                    <button id="convert-contact-btn" class="contact1-form-btn">
                                        <span>
                                            Generate
                                            <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                        </span>
                                    </button>
                            </div>
                                    
                                </form>
                            </div>
                        </div>
                        <div class="col-sm-6">                          
                            <div style="color:#657568;font-size:18px;line-height: 36px;">
                                <i>1.&nbsp;System support <b>XLS</b> and <b>XLXS</b> formats.</i><br />
                                <i>2.&nbsp;Select the source excel file, choose the column for mobile and message to map. Now generate the new excel with sample format.</i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--===============================================================================================-->
    <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/bootstrap/js/popper.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/select2/select2.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/tilt/tilt.jquery.min.js"></script>
    <script >
                                            $('.js-tilt').tilt({
                                                scale: 1.1
                                            })
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
    <script>
                                            window.dataLayer = window.dataLayer || [];
                                            function gtag() {
                                                dataLayer.push(arguments);
                                            }
                                            gtag('js', new Date());

                                            gtag('config', 'UA-23581568-13');



                                            $(document).ready(function () {
                                                $("#wrong-coloumn").css("display", "none");
                                                $("#message").css("display", "none");
                                                $("#contact-error").text('');
                                                $("#coloumn-error").text('');

                                                var url_string = window.location.href;
                                                var url = new URL(url_string);
                                                var error = url.searchParams.get("error");
                                                var status = url.searchParams.get("status");
                                                /* if(status == "success"){
                                                 $("#message-success").css("display","block");
                                                 }*/
                                                if (error == "wrongcoloumn") {
                                                    $("#wrong-coloumn").css("display", "block");
                                                }


                                                $("#convert-contact-btn").click(function () {
                                                    if ($("#convert-excel").val() == "") {
                                                        $("#convert-contact-error").text('Required');
                                                        return false;
                                                    } else {
                                                        var str = $("#convert-excel").val();
                                                        var strArr = str.split('.');
                                                        if (strArr[1] != 'xls' && strArr[1] != 'xlsx') {
                                                            $("#convert-contact-error").text('* Please upload excel in xls or xlsx  format');
                                                            return false;
                                                        } else {
                                                            $("#convert-contact-error").text('');
                                                        }
                                                    }


                                                    if ($("#mobile-coloumn").val() == "" || $("#message-coloumn").val() == "") {
                                                        $("#coloumn-error").text('* Please select both mobile and message coloumn');
                                                        return false;
                                                    } else {
                                                        $("#coloumn-error").text('');
                                                    }


                                                    $("#convertcustom").submit();
                                                });
                                            });

    </script>

    <!--===============================================================================================-->
    <script src="js/main.js"></script>

</body>
</html>
